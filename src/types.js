/**
 * Задача 1: написать типы для обхектов Column и Card
 *
 * структура объекта Column
 * {
 *     id: 1,
 *     title: 'name',
 *     cards: [Card],
 * }
 *
 * Структура Card
 * {
 *     id: 1
 *     title: 'name',
 *     columnId: 1,
 *     column: { Column }
 * }
 *
 * Задача 2: написать resolve методы для получения карточек колонки, и калонки для каждой карточки
 */

import {GraphQLInt, GraphQLNonNull, GraphQLObjectType, GraphQLList, GraphQLString, GraphQLInterfaceType} from "graphql";
import {data} from './mock';

export const Column = new GraphQLObjectType({
    name: 'Column',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        cards: {
            type: null,
            resolve: () => {
            },
        },
    })
});

export const Card = new GraphQLObjectType({
    name: 'Card',
    fields: () => ({
        column: {
            type: null,
            resolve: () => {
            }
        }
    }),
});
