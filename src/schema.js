/**
    Задача 3: написать корень схемы Query, Mutation

    список Query:
        1. Получить все колонки
        2. Получить колонку по id
        3. Получить все карточки
        4. Получить карточку по  id

    список Mutation:
        1. Создать колонку
        2. Обновить карточку

    Задача 4: Написать InputType для создания колоноки и обновления карточки
 */
import {
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLInt,
} from "graphql";
import {Column, Card} from "./types";
import {data} from './mock';

export const schema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            columns: {
                type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(Column))),
                resolve: () => {},
            },
            card: {
                type: new GraphQLNonNull(Card),
                args: {
                    id: { type: new GraphQLNonNull(GraphQLInt) }
                },
                resolve: (parent, args) => {
                    console.log(args) // { id: number }
                },
            },
        },
    }),

    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {},
    }),
});

export function getRandomId() {
    return Math.floor(Math.random() * 1000);
}

