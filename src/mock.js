const mockCards = [
    {
        id: 1,
        title: 'Workshop',
        columnId: 1,
    },
    {
        id: 2,
        title: 'GraphQL',
        columnId: 1,
    },
    {
        id: 3,
        title: 'Apollo',
        columnId: 2,
    },
];

const mockColumns = [
    {
        id: 1,
        title: 'TODO'
    },
    {
        id: 2,
        title: 'Completed'
    },
];

export const data = {
    columns: mockColumns,
    cards: mockCards,
}
